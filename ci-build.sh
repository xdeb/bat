#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\[\033[1;94m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'
else
    PS4='\[\033[1;94m\]$(date +%FT%T%z) ${0}:${LINENO}\n+ \e[m\]'
fi

. /etc/os-release

function cargo-cache() { sh -c "if command -v cargo-cache >/dev/null; then command cargo-cache $* ; fi" ; }
function sccache() { sh -c "if command -v sccache >/dev/null; then command sccache $* ; fi" ; }

set -eux


###########################
##                       ##
##  SET UP BUILDING ENV  ##
##                       ##
###########################

case $ID in
debian)
    case $VERSION_CODENAME in
    bullseye|bookworm)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
esac
export CODENAME

# Prepare `file` command
if ! command -v file >/dev/null; then
    case $CODENAME in
    bullseye|bookworm)
        apt-get update
        apt-get install --no-install-recommends --yes file
        ;;
    *)
        echo "ERROR: CODENAME not found: $CODENAME"
        exit 1
    esac
fi

# Prepare `tree` command
if ! command -v tree >/dev/null; then
    case $CODENAME in
    bullseye|bookworm)
        apt-get update
        apt-get install --no-install-recommends --yes tree
        ;;
    *)
        echo "ERROR: CODENAME not found: $CODENAME"
        exit 1
    esac
fi

case $TARGET_ARCH in
x86_64)
    target=x86_64-unknown-linux-gnu
    ;;
i686)
    target=i686-unknown-linux-gnu

    rustup target add i686-unknown-linux-gnu

    # install cross compile dependencies
    apt-get update
    apt-get install --yes gcc-multilib
    ;;
*)
    echo "ERROR: TARGET_ARCH not found: $TARGET_ARCH"
    exit 1
esac

src_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
dist_dir=$CI_PROJECT_DIR/dist
target_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION/target/$target

# Delete the .git directory to avoid printing the Git commit hash in the output of `bat --version`.
rm -rf .git/

# Restore the building cache
mkdir -p .cache
cache_hash_0=""
if [ -f .cache/${PKG_NAME}-${PKG_VERSION}-$CODENAME-$TARGET_ARCH.tar.gz ]; then
    echo "** restore cache"

    mkdir -p $src_dir/target
    tar x -C $src_dir/target -f .cache/${PKG_NAME}-${PKG_VERSION}-$CODENAME-$TARGET_ARCH.tar.gz

    cache_hash_0=$(find $src_dir -type f -exec md5sum {} \; | md5sum)
fi

cd $src_dir


################
##            ##
##  DO BUILD  ##
##            ##
################

# Build binaries
cargo build --release --target=$target
cargo-cache local
sccache --show-stats

install -Dv $target_dir/release/$PKG_NAME $dist_dir/$TARGET_ARCH/$CODENAME/$PKG_NAME
strip --strip-all --verbose $dist_dir/$TARGET_ARCH/$CODENAME/$PKG_NAME

$dist_dir/$TARGET_ARCH/$CODENAME/$PKG_NAME --version

ldd $dist_dir/$TARGET_ARCH/$CODENAME/$PKG_NAME
file $dist_dir/$TARGET_ARCH/$CODENAME/$PKG_NAME

# FIXME: the 4 install commands below are dirty fix. waiting for cargo-deb to fix the "assets bug".
install -m644 ${target_dir}/release/build/bat-*/out/assets/manual/bat.1 bat.1
install -m644 ${target_dir}/release/build/bat-*/out/assets/completions/bat.bash bat.bash
install -m644 ${target_dir}/release/build/bat-*/out/assets/completions/bat.fish bat.fish
install -m644 ${target_dir}/release/build/bat-*/out/assets/completions/bat.zsh  bat.zsh

mkdir -p $dist_dir
cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~$CODENAME
cargo-cache local
sccache --show-stats

case $CODENAME in
bullseye)
    cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~focal
    ;;
bookworm)
    cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~jammy
    cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~noble
esac

# Check outputs
tree -C $dist_dir/


###############
##           ##
##  DO TEST  ##
##           ##
###############

cargo test --target=$target
cargo-cache local
sccache --show-stats


##############
##          ##
##  FINISH  ##
##          ##
##############

# Create the building cache
cache_hash_1=$(find $src_dir -type f -exec md5sum {} \; | md5sum)
if [ "$cache_hash_0" != "$cache_hash_1" ]; then
    echo "** update cache"
    tar c -C $src_dir/target . | gzip -1 > $CI_PROJECT_DIR/.cache/$PKG_NAME-$PKG_VERSION-$CODENAME-$TARGET_ARCH.tar.gz
else
    echo "** cache not changed, skip to create new one"
fi


exit 0
