#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\[\033[1;94m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'
else
    PS4='\[\033[1;94m\]$(date +%FT%T%z) ${0}:${LINENO}\n+ \e[m\]'
fi

. /etc/os-release

function cargo-cache() { sh -c "if command -v cargo-cache >/dev/null; then command cargo-cache $* ; fi" ; }
function sccache() { sh -c "if command -v sccache >/dev/null; then command sccache $* ; fi" ; }

set -eux


###########################
##                       ##
##  SET UP BUILDING ENV  ##
##                       ##
###########################

case $ID in
alpine)
    CODENAME=$ID
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
esac
export CODENAME

# Prepare `gzip`, `tar` and `tree` command
apk add --no-cache gzip tar tree

case $TARGET_ARCH in
x86_64)
    target=x86_64-unknown-linux-musl
    ;;
*)
    echo "ERROR: TARGET_ARCH not found: $TARGET_ARCH"
    exit 1
esac

src_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
dist_dir=$CI_PROJECT_DIR/dist
target_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION/target/$target

# Delete the .git directory to avoid printing the Git commit hash in the output of `bat --version`.
rm -rf .git/

# Restore the building cache
cache_hash_0=""
if [ -f .cache/${PKG_NAME}-${PKG_VERSION}-alpine-$TARGET_ARCH.tar.gz ]; then
    echo "** restore cache"

    mkdir -p $src_dir/target
    tar x -C $src_dir/target -f .cache/${PKG_NAME}-${PKG_VERSION}-alpine-$TARGET_ARCH.tar.gz

    cache_hash_0=$(find $src_dir -type f -exec md5sum {} \; | md5sum)
fi

cd $src_dir


################
##            ##
##  DO BUILD  ##
##            ##
################

# Build binaries
cargo build --release --target=$target
cargo-cache local
sccache --show-stats

install -Dv $target_dir/release/$PKG_NAME $dist_dir/x86_64/alpine/$PKG_NAME
strip --strip-all --verbose $dist_dir/x86_64/alpine/$PKG_NAME

$dist_dir/x86_64/alpine/$PKG_NAME --version

ldd $dist_dir/x86_64/alpine/$PKG_NAME
file $dist_dir/x86_64/alpine/$PKG_NAME

# Check outputs
tree -C $dist_dir/


###############
##           ##
##  DO TEST  ##
##           ##
###############

cargo test --target=$target
cargo-cache local
sccache --show-stats


##############
##          ##
##  FINISH  ##
##          ##
##############

# Create the building cache
cache_hash_1=$(find $src_dir -type f -exec md5sum {} \; | md5sum)
if [ "$cache_hash_0" != "$cache_hash_1" ]; then
    echo "** update cache"
    tar c -C $src_dir/target . | gzip -1 > $CI_PROJECT_DIR/.cache/${PKG_NAME}-${PKG_VERSION}-alpine-$TARGET_ARCH.tar.gz
else
    echo "** cache not changed, skip to create new one"
fi


exit 0
